return {
    texture = 'Jukebox.png',
    frames = {
            {
                name = "Juke",
                spriteColorRect = { x = -10, y = -10, width = 103, height = 150 },
				uvRect = { u0 = 0.0, v0 = 0.0, u1 = 0.333, v1 = 0.577 },
				spriteSourceSize = { width = 103, height = 130 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "Juke_damaged",
				spriteColorRect = { x = -10, y = -10, width = 103, height = 150 },
				uvRect = { u0 = 0.333, v0 = 0.0, u1 = 0.666, v1 = 0.577 },
				spriteSourceSize = { width = 103, height = 130 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "Juke_destroyed",
				spriteColorRect = { x = -10, y = -10, width = 103, height = 150 },
				uvRect = { u0 = 0.666, v0 = 0.0, u1 = 1.0, v1 = 0.577 },
				spriteSourceSize = { width = 103, height = 130 },
                spriteTrimmed = true,
                textureRotated = false
            },
        }
    } 